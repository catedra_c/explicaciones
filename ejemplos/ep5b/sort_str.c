#include <stdlib.h>  // qsort()
#include <stdio.h>
#include <string.h>
#include "printing.h" // print_all()
/*
void qsort(void *base, size_t nmemb, size_t size,
           int(*compar)(const void *, const void *));
*/

int intcmp(const void *i1, const void *i2){
	const int *n1 = i1;
	const int *n2 = i2;
	return *n1 - *n2;
}

int intcmp_reverso(const void *i1, const void *i2){
	const int *n1 = i1;
	const int *n2 = i2;
	return *n2 - *n1;
}

void print_str(void *str){
	printf("%s, ", *(char **)str);
}

int strcmp2(const void *a, const void *b){
	return strcmp(*(char **)a, *(char **)b);
}

int main(){
	char *array[] = {
		"hola",
		"chau",
		"sdd",
		"aaa",
		"zzz"
	};

	array_each(array, 5,
			   sizeof(char *), print_str);
	putchar('\n');

	qsort(array, sizeof(array) / sizeof(char *),
		  sizeof(char *), strcmp2);

	array_each(array, sizeof(array) / sizeof(char *),
			   sizeof(char *), print_str);
	putchar('\n');
	return EXIT_SUCCESS;
}
