#include "printing.h"

void array_each(void *array, size_t nmemb, size_t size,
		         void (*prnt)(void *data)){
	int i = 0;
	for (i = 0; i < nmemb; i++){
		prnt((char *) array + i * size);
	}
}

void print_int(void *i){
	printf("%d, ", *(int *) i);
}

