#include <stdlib.h>  // qsort()
#include <stdio.h>
#include "printing.h" // print_all()

int intcmp(const void *i1, const void *i2){
	return *(int *)i1 - *(int *)i2;
}

int main(){
	int array[] = {32, 4, 3, 34, 5 ,65, 34, 23, 23, 4, 3,6, 2, 1, -74, 34, -4};
	int buscado = 6;
	int *encontrado;
	print_all(array, int);
	putchar('\n');

	qsort(array, sizeof(array) / sizeof(int), sizeof(int), intcmp);
	encontrado = bsearch(&buscado, array, sizeof(array) / sizeof(int), sizeof(int), intcmp);

	if (encontrado != NULL){
		printf("Encontrado %d\n", *encontrado);
	}
	else{
		puts("No encontrado");
	}

	return EXIT_SUCCESS;
}
