#ifndef PRINTING_H
#define PRINTING_H

#include <stdlib.h>
#include <stdio.h>
#define print_all(array, type) array_each((array), sizeof(array)/sizeof(type),\
		                                  sizeof(type), print_##type)

void array_each(void *array, size_t nmemb, size_t size, void (*prnt)(void *data));
void print_int(void *i);

#endif
