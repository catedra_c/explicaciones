#include <stdlib.h>  // qsort()
#include <stdio.h>
#include "printing.h" // print_all()

int intcmp(const void *i1, const void *i2){
	return *(int *)i1 - *(int *)i2;
}

int main(){
	int array[] = {32, 4, 3, 34, 5 ,65, 34, 23, 23, 4, 3,6, 2, 1, -74, 34, -4};
	
	print_all(array, int);
	putchar('\n');

	qsort(array, sizeof(array) / sizeof(int), sizeof(int), intcmp);

	print_all(array, int);
	putchar('\n');

	return EXIT_SUCCESS;
}
