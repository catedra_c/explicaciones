#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

int sumatoria(int cantidad, ...){
	int i, suma = 0;
	va_list ap;

	va_start(ap, cantidad);
	for (i = 0; i < cantidad; i++){
		suma += va_arg(ap, int);
	}
	va_end(ap);
	return suma;
}

int main(){
	int res;
	res = sumatoria(2, 1, 2);
	printf("%d\n", res);
	res = sumatoria(4, 1, 2, 3, 4);
	printf("%d\n", res);
	res = sumatoria(1, 2);
	printf("%d\n", res);
	return EXIT_SUCCESS;
}

