#include <stdio.h>
#include <string.h>
#include <stdlib.h>
/*
 * copiar() retorna la dirección de memoria de un nuevo
 * string alocado con malloc() esta es la versión
 * correcta
 */
char *copiar(char *string){
    int tam = strlen(string);
    char *buf = malloc(tam + 1);
    strcpy(buf, string);
    return buf;
}

/* Nunca hay que retornar un puntero a una variable
 * local copiar2() es incorrecto
 */
char *copiar2(char *string){
    char buf[strlen(string) + 1];
    strcpy(buf, string);
    return buf;
}

int main(){
    char *hola = "asdds";
    char *x;
    x = copiar(hola);

    // Al terminar de usar x hay que liberarlo con
    // free (ya que fue alocado dinámicamente)
    free(x);
    return 0;
}



