/* GIMP RGB C-Source image dump (sad.c) */

#define TMP_WIDTH (3)
#define TMP_HEIGHT (4)
#define TMP_BYTES_PER_PIXEL (3) /* 3:RGB, 4:RGBA */
#define TMP_PIXEL_DATA ((unsigned char*) TMP_pixel_data)

// Acá le saqué el static, de otra manera no se
// puede usar esta función en imprimir.c
const unsigned char TMP_pixel_data[3 * 4 * 3 + 1] =
("\7\7\7\25\25\25""111\25\25\25###<<<111<<<OOOXXX```rrr");

