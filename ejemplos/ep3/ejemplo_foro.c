#include <stdio.h>
#include <string.h>

#ifdef DEBUG
    #define APRINT(var) printf("La dirección de memoria de %-6s es %p\n", #var, &(var))
#else
    #define APRINT(var)
#endif


int login(char *user, char *passwd){
    int entrar = 0;
    char p[6];
    char u[6];
    printf("Usuario: "); gets(u);
    printf("Password: "); gets(p);
    APRINT(entrar); APRINT(p); APRINT(u);
    if (strcmp(user, u) == 0 && strcmp(passwd, p) == 0){
        entrar = 1;
    }
    return entrar;
}

int main(){
    char user[] = "admin";
    char passwd[] = "12345";
    if (login(user, passwd)){
        puts("Pudo ingresar con el usuario admin");
    }
    else{
        puts("Nombre de usuario o password incorrecto");
    }
    return 0;
}
