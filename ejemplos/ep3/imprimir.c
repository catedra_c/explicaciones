#include <stdio.h>

// A estas macros las copié desde el .c de la imagen
// después les voy a armar los .h para que no tengan
// que hacer esto
#define TMP_WIDTH (3)
#define TMP_HEIGHT (4)
// Con el .h también se van a ahorrar esto:
extern const unsigned char TMP_pixel_data[3 * 4 * 3 + 1];

int main(){
    int i, j;
    for (i = 0; i < TMP_HEIGHT; i++){
        for (j = 0; j < TMP_WIDTH * 3; j = j + 3){
            printf("%x %x %x ",
                   TMP_pixel_data[i * TMP_WIDTH * 3 + j],
                   TMP_pixel_data[i * TMP_WIDTH * 3 + j+1],
                   TMP_pixel_data[i * TMP_WIDTH * 3 + j+2]);
        }
    }

    return 0;
}
