/*
 * Lea líneas, puede asumir que las líneas son de a lo sumo 1023
 * chars contando el \n
 */

char string[1024];
char *string2; // 0
int main(){
	string2 = malloc(1024);
	if (string2 == NULL) exit(EXIT_FAILURE);
	fgets(string2, 1024, stdin);
	free(string2);
	return EXIT_SUCCESS;
}
