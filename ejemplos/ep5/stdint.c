#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>

int main(){
	uint32_t x = 24;
	printf("%d\n", sizeof(x));
	printf("%" PRIu32 "\n", x);
	return 0;
}
