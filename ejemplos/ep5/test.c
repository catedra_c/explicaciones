#include <stdio.h>
#include <stdlib.h>
int main(){
	FILE *f = fopen("archivo", "r");
	char *lineas = NULL;
	size_t alocado = 0;
	if (f == NULL){
		puts("Fallo al abrir");
		return 1;
	}
	while (getline(&lineas, &alocado, f) != -1){
		char *fin, *actual = lineas;
		long res;
		while (1){
			res = strtol(actual, &fin, 10);
			if (actual == fin){
				break;
			}
			printf("Se leyo %d\n", res);
			actual = fin;
		}
	}
	free(lineas);
	fclose(f);
}
