#include <stdio.h>
#include "pila.h"

int main(int argc, char **argv){
	pila_t numeros = pila_crear();
	for (int i = 0; i < 10; i++){
		push(&numeros, i);
	}
	while (pila_tam(numeros) > 0){
		printf("Número %d\n", pop(&numeros));
	}
	return 0;
}
