#include "lista.h"

lista_t lista_crear(){
	lista_t tmp;
	tmp.primero = tmp.ultimo = NULL;
	tmp.tam = 0;
	return tmp;
}

void lista_agregar(lista_t *lista, TIPO_DE_DATO dato){
	nodo_t *nuevo = malloc(sizeof(nodo_t));
	nuevo->dato = dato;
	nuevo->siguiente = NULL;
	if (lista->tam == 0){
		lista->primero = lista->ultimo = nuevo;
	}
	else{
		lista->ultimo->siguiente = nuevo;
		lista->ultimo = nuevo;
	}
	lista->tam++;
}

int lista_eliminar(lista_t *lista, int indice){
	lista_iterador_t previo = NULL;
	lista_iterador_t iter = lista_iterador(*lista);
	nodo_t *nprevio, *nactual;
	for (int i = 0; i < indice && iter != NULL; i++){
		previo = iter;
		iter = lista_iterador_siguiente(iter);
	}
	if (iter == NULL) return 0;
	nprevio = lista_iterador_nodo(previo);
	nactual = lista_iterador_nodo(iter);
	if (lista_tam(*lista) == 1){
		lista->primero = lista->ultimo = NULL;
	}
	else if (nactual == lista->primero){
		lista->primero = nactual->siguiente;
	}
	else if (nactual == lista->ultimo){
		lista->ultimo = nprevio;
		lista->ultimo->siguiente = NULL;
	}
	else{
		nprevio->siguiente = nactual->siguiente;
	}
	lista->tam--;
	free(nactual);
	return 1;
}
int lista_tam(lista_t lista){
	return lista.tam;
}

// Iterador para listas
lista_iterador_t lista_iterador(lista_t lista){
	return lista.primero;
}
lista_iterador_t lista_iterador_siguiente(lista_iterador_t iter){
	return iter->siguiente;
}
nodo_t *lista_iterador_nodo(lista_iterador_t iter){
	return iter;
}
