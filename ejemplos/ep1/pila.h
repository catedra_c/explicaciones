#ifndef _PILA_H_
#define _PILA_H_
#include "lista.h"

typedef lista_t pila_t;

pila_t pila_crear();
void push(pila_t *, TIPO_DE_DATO);
TIPO_DE_DATO pop(pila_t *);
TIPO_DE_DATO top(pila_t);
int pila_tam(pila_t);

#endif
