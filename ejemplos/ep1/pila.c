#include "pila.h"

pila_t pila_crear(){
	return lista_crear();
}
void push(pila_t *pila, TIPO_DE_DATO dato){
	lista_agregar(pila, dato);
}
TIPO_DE_DATO pop(pila_t *pila){
	TIPO_DE_DATO dato = pila->ultimo->dato;
	lista_eliminar(pila, pila_tam(*pila) - 1);
	return dato;
}
TIPO_DE_DATO top(pila_t pila){
	return pila.ultimo->dato;
}
int pila_tam(pila_t pila){
	return lista_tam(pila);
}
