#ifndef _LISTA_H_
#define _LISTA_H_
#include <stdlib.h>

typedef struct nodo{
	TIPO_DE_DATO dato;
	struct nodo *siguiente;
} nodo_t;

typedef struct lista{
	nodo_t *primero;
	nodo_t *ultimo;
	int tam;
} lista_t;

lista_t lista_crear();
void lista_agregar(lista_t*, TIPO_DE_DATO);
int lista_eliminar(lista_t*, int);
int lista_tam(lista_t);

typedef nodo_t *lista_iterador_t;
lista_iterador_t lista_iterador(lista_t);
lista_iterador_t lista_iterador_siguiente(lista_iterador_t);
nodo_t *lista_iterador_nodo(lista_iterador_t);

#endif
