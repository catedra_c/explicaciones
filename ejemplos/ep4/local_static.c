#include <stdio.h>
void funcion () {
	static int x = 0;
	int y = 0;
	x++; y++;
	printf("x = %d , y = %d\n", x, y);
}

int main(){
	int i;
	for (i = 0; i < 10; i++){
		funcion();
	}
	return 0;
}
