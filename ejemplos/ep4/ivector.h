#ifndef _CVECTOR_H_
#define _CVECTOR_H_

#include <stdlib.h>
#define GROW_FACTOR 1024
#define SHRINK_FACTOR GROW_FACTOR / 2

struct ivector {
	int *data; ///< Buffer de datos
	int size; ///< Tamaño usado del vector
	int reserved; ///< Tamaño alocado para el vector
};

typedef struct ivector ivector_t;

ivector_t ivector_new();
void ivector_append(ivector_t *vector, int element);
int ivector_remove_last(ivector_t *vector);
void ivector_free(ivector_t *vector);
#define ivector_push(v, e) ivector_append((v), (e))
#define ivector_pop(v) ivector_remove_last((v))
char *ivector_as_string(ivector_t vector);

#endif
