typedef struct {
	int top;
	int valores[200];
} pila_t;

pila_t crear_pila();
int push_pila(pila_t *pila, int valor);
int pop_pila(pila_t *pila);
void destruir_pila(pila_t *pila);



