#include <stdio.h>

int main(){
	int y;
	int leidos;
	while ((leidos = scanf("%d", &y)) != EOF){
		if (leidos == 0){
			puts("Hay un error");
			while (getchar() != '\n');
		}
		else {
			printf("%d\n", y);
		}
	}
	return 0;
}
