#include <stdio.h>
#include <stdlib.h> // Para usar strtol
// para leer un número en punto flotante ver "man strtod"

int main(){
	// Precondición: las lineas no son de más de 1023 bytes
	char x[1024];
	char *y;
	y = x;
	int num1; // leo un hexa
	int num2; // leo decimal
	if (fgets(x, 1024, stdin) == NULL){
		puts("Fin de archivo o error");
		// Para saber si es error o fin de archivo ver "man feof" y "man ferror"
		return 1;
	}
	num1 = strtol(x, &y, 16);
	if (x == y){
		puts("No pude convertir el primero");
		return 2;
	}
	if (*y != ' '){
		puts("Basura despues del valor 1");
		return 3;
	}
	// Leo el valor num2
	num2 = strtol(y, &y, 10);
	if (x == y){
		puts("No pude convertir el segundo");
		return 4;
	}
	if (*y != '\n'){
		puts("Basura despues del valor 2");
		return 5;
	}
	printf("num1 = %d, num2 = %d\n", num1, num2);
	return 0;
}
