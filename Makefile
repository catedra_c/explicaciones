DOCUMENTS=$(patsubst %.tex,%.pdf,$(wildcard *.tex))
HANDOUTS=$(patsubst %.tex,%_handout.pdf,$(wildcard *.tex))

all: $(DOCUMENTS) $(HANDOUTS)

%.pdf: %.tex generate_figures
	pdflatex -jobname "$*" "\input{style/presentation.sty}"

%_handout.pdf: %.tex generate_figures handouts
	pdflatex -output-directory handouts -jobname "$*" "\input{style/handout.sty}"
	mv "handouts/$*.pdf" "handouts/$*_handout.pdf"

%_wide.pdf: %.tex generate_figures wide
	pdflatex -output-directory wide -jobname "$*" "\input{style/wide.sty}"

%: Explicación_de_Práctica_%.pdf
	#

%h: Explicación_de_Práctica_%_handout.pdf
	#

%wide: Explicación_de_Práctica_%_wide.pdf
	#

generate_figures:
	make -C images
	make -C style

pdfonly: all
	rm -f *.nav *.out *.snm *.toc *.vrb *.aux *.bak *.log
	make -C handouts pdfonly
	make -C wide pdfonly

clean:
	make -C images clean
	make -C style clean
	make -C handouts clean
	make -C wide clean
	rm -f *.nav *.out *.pdf *.snm *.toc *.vrb *.log *.aux

reallyclean: clean
	rm -f *.aux *.bak *.log
	make -C handouts reallyclean
	make -C wide reallyclean

PRINTH=printf '%-19s %s\n'
help:
	@echo ====================================Targets=====================================
	@$(PRINTH) 'all' 'construye todo'
	@$(PRINTH) 'pdfonly:' 'compila y luego borra los archivos auxiliares'
	@$(PRINTH) 'N:' 'compila la explicación N'
	@$(PRINTH) 'Nh:' 'compila el handout de la explicación N'
	@$(PRINTH) 'Nwide': 'compila una versión widescreen de la explicación N'
	@$(PRINTH) 'generate_figures:' 'compila el contenido de images/ y style/'
	@$(PRINTH) 'clean:' 'elimina algunos archivos generados en la compilación'
	@$(PRINTH) 'reallyclean:' 'elimina todo salvo los fuentes'

.PHONY: clean generate_figures help
